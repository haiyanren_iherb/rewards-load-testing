import http from "k6/http";
import {check, sleep, group} from "k6";

const api_path = "https://css.app.iherbpreprod.com/config/switch-flags";
// the api response time is about 320ms in preprod from office network

export const options = {
    stages: [
        { duration: '2s', target: 1 },
        { duration: '10s', target: 5 },
        { duration: '20s', target: 10 },
        { duration: '1m', target: 20 },
        { duration: '5m', target: 50 }
    ],
  };
  
  export default function () {
    let headers = { "Content-Type": "application/json"};
    const payload = JSON.stringify({
        "hide-email-form": {"queryString": "", "timezoneOffset": -480}
      });
    
    const res = http.post(api_path, payload, { headers: headers });
    check(res, { 'status was 200': (r) => r.status == 200 });
    sleep(1);
  }